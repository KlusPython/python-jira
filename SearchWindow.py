from tkinter import *
import tkinter.messagebox as tm
from jira.client import JIRA
import Auth

class LoginFrame(Frame):
    def __init__(self, master):
        super().__init__(master)

        self.label_JQL = Label(self, text="Input JQL Here:")
        self.label_Result = Label(self, text="Result")
        self.entry_JQL = Entry(self)


        self.label_JQL.grid(row=0, sticky=E)
        self.entry_JQL.grid(row=0, column=1)
        self.label_Result.grid(row=2, sticky=E)

        self.logbtn = Button(self, text="Search", command=self._login_btn_clicked)
        self.logbtn.grid(columnspan=2)

        self.pack()
    def JIRA_Search(self):
        jqlresult = self.entry_JQL.get()
        a = Auth.auth("admin", "admin")
        jira_options = {'server': 'http://localhost:8080'}
        jira = JIRA(options=jira_options, basic_auth=(a.username, a.password))
        issues_in_project = jira.search_issues(jqlresult)

        print(issues_in_project)
        return issues_in_project

    def _login_btn_clicked(self):
        search = self.JIRA_Search()
        self.label_password = Label(self, text=str(search))
        self.label_password.grid(row=1, sticky=E)


root = Tk()
lf = LoginFrame(root)
root.mainloop()